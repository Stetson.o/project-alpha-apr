from django.urls import path
from tasks.views import create_task, mine

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", mine, name="show_my_tasks"),
]
